
int disk_open(const char *dev,int flag);
void flush_buffer_cache (int fd);
void disk_close(int fd);
int io_write(int fd, char *buf, unsigned int req_size, unsigned long offset);
int io_read(int fd, char *buf, unsigned int req_size, unsigned long long offset);
