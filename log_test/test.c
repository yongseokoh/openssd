#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include "list.h"
#include "cache.h"
#include "disk_io.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#define COMMAND_NORMAL		0
#define COMMAND_LOGWRITE	1
#define COMMAND_DELTAWRITE	2
#define COMMAND_LOGGC		3

#define LOGSIZE 			(512*1024*2) // 256MB
//#define LOGSIZE 			(1024*1024*2) // 1024MB
#define LOGWRITE_START		2048 //sectors
#define LOGWRITE_LENGTH		(LOGSIZE) 
#define LOGGC_START			(LOGWRITE_START+LOGWRITE_LENGTH)//sectors
#define LOGGC_LENGTH		(LOGSIZE)
#define DATA_START			(LOGGC_START+LOGGC_LENGTH)
#define DATA_LENGTH			(2*1024*1024*2)

#define SECTOR_SIZE 512
#define PAGE_SIZE (32*1024)
#define SECTORS_PER_PAGE (PAGE_SIZE/SECTOR_SIZE)

unsigned long genrand();
#define RND(x) ((x>0)?(genrand() % (x)):0)

unsigned int log_map[LOGWRITE_LENGTH/SECTORS_PER_PAGE];
unsigned int log_count;
unsigned int log_ptr;
struct cache_manager *lru_manager;


/* allocate a alignment-bytes aligned buffer */
void *allocate_aligned_buffer(size_t size)
{
	void *p;

	p=(void *)memalign(getpagesize(), size);
	if(!p) {
		perror("memalign");
		exit (0);
		return NULL;
	}
	return p;
}

int main(int argc, char *argv[]){	
	struct lru_node *ln = NULL;
	unsigned int blkno;
	unsigned int i, num_writes;;
	int fd;
	char *buf;
	unsigned int *p;

	log_count = 0;
	log_ptr = 0;
	memset(log_map, ~0, sizeof(unsigned int)*(LOGWRITE_LENGTH/SECTORS_PER_PAGE));


	if(argc<=2){
		printf(" usage: log_test /dev/sdb num_writes(KB)\n");
		return -1;
	}

	fd = disk_open(argv[1], O_RDWR|O_DIRECT);
	if(fd<0){
		perror(" disk open error ");
		return -1;
	}

	buf = allocate_aligned_buffer(PAGE_SIZE);
	if(!buf){
		perror(" mem alloc error ");
		return -1;
	}
	p = (unsigned int *)buf;

	printf(" LogWrite	Partition start = %dGB length = %dGB\n", LOGWRITE_START/2/1024/1024, LOGWRITE_LENGTH/2/1024/1024);
	printf("    LogGC	Partition start = %dGB length = %dGB\n", LOGGC_START/2/1024/1024, LOGGC_LENGTH/2/1024/1024);
	printf("    Data	Partition start = %dGB length = %dGB\n", DATA_START/2/1024/1024, DATA_LENGTH/2/1024/1024);

	lru_init(&lru_manager,"LRU", LOGWRITE_LENGTH/SECTORS_PER_PAGE, LOGWRITE_LENGTH/SECTORS_PER_PAGE, 1, 0);

	num_writes = (unsigned int)((unsigned long long)atoi(argv[2])*1024/PAGE_SIZE);
	printf(" Workload %uKB, num pages to write = %u\n", atoi(argv[2]), num_writes);
	for(i = 0;i < num_writes;i++){
		blkno = RND(DATA_LENGTH/SECTORS_PER_PAGE) + DATA_START/SECTORS_PER_PAGE;
		ln = CACHE_SEARCH(lru_manager, blkno);
		if(ln){ // invalidate previsoud log 
			log_map[ln->cn_log_blkno] = ~0;
			log_count--;
			CACHE_REMOVE(lru_manager, ln);
			free(ln);
		}

		if(blkno<DATA_START/SECTORS_PER_PAGE){
			printf(" blkno error = %u\n", blkno);
			return 0;
		}
		// alloc log 
		ln = CACHE_ALLOC(lru_manager, NULL, blkno);
		ln->cn_log_blkno = log_ptr; 
		CACHE_INSERT(lru_manager, ln);
		log_map[log_ptr] = blkno;
		log_ptr = (log_ptr + 1) % (LOGWRITE_LENGTH/SECTORS_PER_PAGE);
		log_count++;

		
		buf[0] = 'r';
		buf[1] = 'a';
		buf[2] = 'i';
		buf[3] = 'd';

		p[1] = COMMAND_LOGWRITE;
		p[2] = ln->cn_org_blkno;
		io_write(fd, buf, PAGE_SIZE, ln->cn_log_blkno * PAGE_SIZE);
	//	printf(" log write = %u, org = %u\n", ln->cn_log_blkno, ln->cn_org_blkno);

		// log cleaning
		if(log_map[log_ptr]!=~0){
	//		printf(" log cleaning = %u\n", log_ptr);
			ln = CACHE_SEARCH(lru_manager, log_map[log_ptr]);
			CACHE_REMOVE(lru_manager, ln);
			free(ln);
			log_map[log_ptr] = ~0;
			log_count--;

			// read delta
			p[1] = COMMAND_LOGGC;
			p[2] = 0;
			io_read(fd, buf, PAGE_SIZE, (unsigned long long)LOGGC_START * SECTOR_SIZE + ln->cn_log_blkno * PAGE_SIZE);

#if 1
			// write delta to the original location
			p[1] = COMMAND_DELTAWRITE;
			p[2] = 0;
			io_write(fd, buf, PAGE_SIZE, (unsigned long long)DATA_START * SECTOR_SIZE  + ln->cn_log_blkno * PAGE_SIZE);
#endif
		}
	}

	//CACHE_PRINT(lru_manager, stdout);
	CACHE_CLOSE(lru_manager);
	disk_close(fd);
	free(buf);
	return 0;
}


#if 0 
int main(){	
	struct cache_manager *lru_manager;
	int i;

	lru_init(&lru_manager,"LRU", 500, 500, 1, 0);

	for(i =0;i < 10000000;i++){
		struct lru_node *ln = NULL;
		unsigned int blkno = rand()%100;

		ln = CACHE_SEARCH(lru_manager, blkno);
		if(!ln){
			ln = CACHE_REPLACE(lru_manager, 0, FCL_REPLACE_ANY);
			ln = CACHE_ALLOC(lru_manager, ln, blkno);
			CACHE_INSERT(lru_manager, ln);
		}else{
			ln = CACHE_REMOVE(lru_manager, ln);
			CACHE_INSERT(lru_manager,
					ln);
		}
	}	

	CACHE_PRINT(lru_manager, stdout);
	CACHE_CLOSE(lru_manager);

	return 0;
}
#endif 

#define N 624
#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */   
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* Initializing the array with a seed */
void
sgenrand(seed)
unsigned long seed;	
{
	int i;

	for (i=0;i<N;i++) {
		mt[i] = seed & 0xffff0000;
		seed = 69069 * seed + 1;
		mt[i] |= (seed & 0xffff0000) >> 16;
		seed = 69069 * seed + 1;
	}
	mti = N;
}

/* Initialization by "sgenrand()" is an example. Theoretically,      */
/* there are 2^19937-1 possible states as an intial state.           */
/* This function allows to choose any of 2^19937-1 ones.             */
/* Essential bits in "seed_array[]" is following 19937 bits:         */
/*  (seed_array[0]&UPPER_MASK), seed_array[1], ..., seed_array[N-1]. */
/* (seed_array[0]&LOWER_MASK) is discarded.                          */ 
/* Theoretically,                                                    */
/*  (seed_array[0]&UPPER_MASK), seed_array[1], ..., seed_array[N-1]  */
/* can take any values except all zeros.                             */
void
lsgenrand(seed_array)
unsigned long seed_array[]; 
/* the length of seed_array[] must be at least N */
{
	int i;

	for (i=0;i<N;i++) 
		mt[i] = seed_array[i];
	mti=N;
}

unsigned long 
genrand()
{
	unsigned long y;
	static unsigned long mag01[2]={0x0, MATRIX_A};
	/* mag01[x] = x * MATRIX_A  for x=0,1 */

	if (mti >= N) { /* generate N words at one time */
		int kk;

		if (mti == N+1)   /* if sgenrand() has not been called, */
			sgenrand(4357); /* a default initial seed is used   */

		for (kk=0;kk<N-M;kk++) {
			y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
			mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1];
		}
		for (;kk<N-1;kk++) {
			y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
			mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1];
		}
		y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
		mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1];

		mti = 0;
	}

	y = mt[mti++];
	y ^= TEMPERING_SHIFT_U(y);
	y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
	y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
	y ^= TEMPERING_SHIFT_L(y);

	return y; 
}
