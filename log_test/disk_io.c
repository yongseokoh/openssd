#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>         // O_WRONLY
#include <errno.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef ENOIOCTLCMD
#	define ENOIOCTLCMD ENOTTY
#endif

typedef char                    __s8;   
typedef short                   __s16;  
typedef int                     __s32;  

typedef long long					__s64;
typedef unsigned char           __u8;  
typedef unsigned short          __u16; 
typedef unsigned int            __u32; 
typedef unsigned long long			__u64;

int disk_open(const char *dev,int flag){
	int fd;
	
	fd = open(dev, flag);
	if(fd < 0)
		printf("device open error\n");

//	printf("open fd = %d\n",fd);

	return fd;
}

void flush_buffer_cache (int fd)
{
	fsync (fd);				/* flush buffers */
	if (ioctl(fd, BLKFLSBUF, NULL))		/* do it again, big time */
		perror("BLKFLSBUF failed");
	/* await completion */
	//if (do_drive_cmd(fd, NULL) && errno != EINVAL && errno != ENOTTY && errno != ENOIOCTLCMD)
	//	perror("HDIO_DRIVE_CMD(null) (wait for flush complete) failed");
}

void disk_close(int fd){
	flush_buffer_cache(fd);
	close(fd);
}


int io_write(int fd, char *buf, unsigned int req_size, unsigned long offset){

	__u64 temp = (__u64)offset;

  	if(lseek64(fd, temp, SEEK_SET)==-1){
		printf("io_write: lseek is error %llu\n",temp);
		return -1;		
	}

	if(write(fd, buf, req_size) < 0){
		printf("io_write: write error\n");
		return -1;
	}
    return 0;
}

int io_read(int fd, char *buf, unsigned int req_size, unsigned long long offset){

	__u64 temp = (__u64)offset;

  	if(lseek64(fd, temp, SEEK_SET)==-1){
		//printf("lseek is error %llu\n",temp);
		return -1;		
	}

	if(read(fd, buf, req_size) < 0){
		//printf("read error %llu\n", temp);
		return -1;
	}


    return 0;
}
